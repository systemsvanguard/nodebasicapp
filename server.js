// import needed libraries, & initialize express
import express from "express";
const port = process.env.PORT || 8088 ; 

const app = express();

// basic routing
import Router from "./routes/main.routes.js";
app.use(Router);


// start app & listen on web port 
app.listen(port, () => console.log(`Web app started & now running on port ${port} at localhost:${port}/. Enjoy!   ` )  );
