import express from "express";

const router = express.Router();

// basic routing
router.get('/', (req, res) => {
    res.send("Welcome to the Home Page" );
} );

router.get('/about', (req, res) => {
    res.send("Welcome to the About Page" );
} );

router.get('/contact', (req, res) => {
    res.send("Welcome to the Contact Page" );
} );

// export 
export default router;
